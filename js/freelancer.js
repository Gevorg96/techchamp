(function($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 70)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Scroll to top button appear
  $(document).scroll(function() {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 100) {
      $('.scroll-to-top').fadeIn();
    } else {
      $('.scroll-to-top').fadeOut();
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 80
  });

  // Collapse Navbar
  var navbarCollapse = function() {
    if ($("#mainNav").offset().top > 100) {
      $("#mainNav").addClass("navbar-shrink");
    } else {
      $("#mainNav").removeClass("navbar-shrink");
    }
  };
  // Collapse now if page is not at top
  navbarCollapse();
  // Collapse the navbar when page is scrolled
  $(window).scroll(navbarCollapse);

  // Modal popup$(function () {
  $('.portfolio-item').magnificPopup({
    type: 'inline',
    preloader: false,
    focus: '#username',
    modal: true
  });
  $(document).on('click', '.portfolio-modal-dismiss', function(e) {
    e.preventDefault();
    $.magnificPopup.close();
  });

  // Floating label headings for the contact form
  $(function() {
    $("body").on("input propertychange", ".floating-label-form-group", function(e) {
      $(this).toggleClass("floating-label-form-group-with-value", !!$(e.target).val());
    }).on("focus", ".floating-label-form-group", function() {
      $(this).addClass("floating-label-form-group-with-focus");
    }).on("blur", ".floating-label-form-group", function() {
      $(this).removeClass("floating-label-form-group-with-focus");
    });
  });

})(jQuery); // End of use strict


$(function() {

  /** ---------------------------- //
   *  @group viewport trigger script 
   * for adding or removing classes from elements in view within viewport
   *  @author @david
   *  use like this: add following to css stylesheets:    
          .foobar.in-view {
          @extend .fadeInUpBig;
          transform:rotate(90deg)}
      */

    // ps: disable on small devices!
  var $animationElements = $('.animation-element');
  var $window = $(window);

  // ps: Let's FIRST disable triggering on small devices!
  var isMobile = window.matchMedia("only screen and (max-width: 768px)");
  if (isMobile.matches) {
      $animationElements.removeClass('animation-element');
  }

  function checkIfInView() {

      var windowHeight = $window.height();
      var windowTopPosition = $window.scrollTop();
      var windowBottomPosition = (windowTopPosition + windowHeight);

      $.each($animationElements, function () {
          var $element = $(this);
          var elementHeight = $element.outerHeight();
          var elementTopPosition = $element.offset().top;
          var elementBottomPosition = (elementTopPosition + elementHeight);

//check to see if this current container is within viewport
          if ((elementBottomPosition >= windowTopPosition) &&
              (elementTopPosition <= windowBottomPosition)) {
              $element.addClass('in-view');
          } else {
              $element.removeClass('in-view');
          }
      });
  }

  $window.on('scroll resize', checkIfInView);
  $window.trigger('scroll');


  /* @end viewport trigger script  */

});

function copyFunction() {
  var copyText = document.getElementById("myInput");
  copyText.select();
  copyText.setSelectionRange(0, 99999)
  document.execCommand("copy");
  alert("Copied the text: " + copyText.value);
}